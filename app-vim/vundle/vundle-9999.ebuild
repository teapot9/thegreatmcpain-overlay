# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit vim-plugin git-r3

DESCRIPTION="Vundle, the plug-in manager for Vim"
HOMEPAGE="https://github.com/VundleVim/Vundle.vim"
EGIT_REPO_URI="https://github.com/VundleVim/Vundle.vim"
LICENSE="MIT"

VIM_PLUGIN_HELPFILES="vundle"

src_install() {
	rm -r test

	vim-plugin_src_install
}
